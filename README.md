# podman-compose-repro-demo

Toto je demo chyby, která nastane na strojích nymfe*.

## Jak reprodukovat
- nainstalovat `podman-compose` přes pip3 (`pip3 install podman-compose`) => mělo by to nainstalovat verzi 1.0.6
- přidat pip3 executables do PATH `export PATH=$PATH:/home/_INSERT_XLOGIN_/.local/bin`
- v této složce spustit `podman-compose up`, měl by vyskočit následující podobný log

```
nymfe105:/home/xfalesn/podman-compose-repro-demo>$ podman-compose up
podman-compose version: 1.0.6
['podman', '--version', '']
using podman version: 3.4.4
** excluding:  set()
['podman', 'ps', '--filter', 'label=io.podman.compose.project=podman-compose-repro-demo', '-a', '--format', '{{ index .Labels "io.podman.compose.config-hash"}}']
['podman', 'network', 'exists', 'podman-compose-repro-demo_default']
['podman', 'network', 'create', '--label', 'io.podman.compose.project=podman-compose-repro-demo', '--label', 'com.docker.compose.project=podman-compose-repro-demo', 'podman-compose-repro-demo_default']
['podman', 'network', 'exists', 'podman-compose-repro-demo_default']
WARN[0000] Error validating CNI config file /home/xfalesn/.config/cni/net.d/podman-compose-repro-demo_default.conflist: [plugin bridge does not support config version "1.0.0" plugin portmap does not support config version "1.0.0" plugin firewall does not support config version "1.0.0" plugin tuning does not support config version "1.0.0"]
podman create --name=podman-compose-repro-demo_nginx_1 --label io.podman.compose.config-hash=a7790a063127c802a419c0ac300e935efbea38cf2467b0165802f32c83a1d4b9 --label io.podman.compose.project=podman-compose-repro-demo --label io.podman.compose.version=1.0.6 --label PODMAN_SYSTEMD_UNIT=podman-compose@podman-compose-repro-demo.service --label com.docker.compose.project=podman-compose-repro-demo --label com.docker.compose.project.working_dir=/home/xfalesn/podman-compose-repro-demo --label com.docker.compose.project.config_files=podman-compose.yml --label com.docker.compose.container-number=1 --label com.docker.compose.service=nginx --net podman-compose-repro-demo_default --network-alias nginx docker.io/library/nginx
WARN[0000] Error validating CNI config file /home/xfalesn/.config/cni/net.d/podman-compose-repro-demo_default.conflist: [plugin bridge does not support config version "1.0.0" plugin portmap does not support config version "1.0.0" plugin firewall does not support config version "1.0.0" plugin tuning does not support config version "1.0.0"]
30856b4c99c2432e75b5e28b926c8edb3450f822ab7e8ad1c7ee7cf737495c3a
exit code: 0
podman start -a podman-compose-repro-demo_nginx_1
WARN[0000] Error validating CNI config file /home/xfalesn/.config/cni/net.d/podman-compose-repro-demo_default.conflist: [plugin bridge does not support config version "1.0.0" plugin portmap does not support config version "1.0.0" plugin firewall does not support config version "1.0.0" plugin tuning does not support config version "1.0.0"]
WARN[0000] Error validating CNI config file /home/xfalesn/.config/cni/net.d/podman-compose-repro-demo_default.conflist: [plugin bridge does not support config version "1.0.0" plugin portmap does not support config version "1.0.0" plugin firewall does not support config version "1.0.0" plugin tuning does not support config version "1.0.0"]
ERRO[0000] error loading cached network config: network "podman-compose-repro-demo_default" not found in CNI cache
WARN[0000] falling back to loading from existing plugins on disk
WARN[0000] Error validating CNI config file /home/xfalesn/.config/cni/net.d/podman-compose-repro-demo_default.conflist: [plugin bridge does not support config version "1.0.0" plugin portmap does not support config version "1.0.0" plugin firewall does not support config version "1.0.0" plugin tuning does not support config version "1.0.0"]
ERRO[0000] Error tearing down partially created network namespace for container 30856b4c99c2432e75b5e28b926c8edb3450f822ab7e8ad1c7ee7cf737495c3a: CNI network "podman-compose-repro-demo_default" not found
Error: unable to start container 30856b4c99c2432e75b5e28b926c8edb3450f822ab7e8ad1c7ee7cf737495c3a: error configuring network namespace for container 30856b4c99c2432e75b5e28b926c8edb3450f822ab7e8ad1c7ee7cf737495c3a: CNI network "podman-compose-repro-demo_default" not found
exit code: 125
nymfe105:/home/xfalesn/podman-compose-repro-demo>$
```

Když se pogooglí, tak se narazí na tiket na ubuntu, který není opravený už několik měsíců a je to known problém na podmanu. https://bugs.launchpad.net/ubuntu/+source/libpod/+bug/2024394

Problém je v tom, že se vygeneruje network config s verzí pluginu 1.0.0, ale ten software, který je na ubuntu LTS 22.04 umí spustit jenom 0.4.0.

### Temporary řešení

V logu `podman-compose` vidíme přímo odkazovaný nevalidní config soubor `/home/xfalesn/.config/cni/net.d/podman-compose-repro-demo_default.conflist`, ten stačí otevřít a `cniVersion` změnit na 0.4.0 a poté spustit znovu `podman-compose`. Toto ale není uplně pro produkční prostřední dobré řešení.

V tiketu co jsem ukázal výše je zmíněno několik řešení. Za mě jsem ještě zkoušel na čisté instalaci Ubuntu LTS 22.04 toto [komentář](https://bugs.launchpad.net/ubuntu/+source/libpod/+bug/2024394/comments/12):

```
sudo mkdir -p /etc/apt/keyrings
curl -fsSL "https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_$(lsb_release -rs)/Release.key" \
  | gpg --dearmor \
  | sudo tee /etc/apt/keyrings/devel_kubic_libcontainers_unstable.gpg > /dev/null
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/devel_kubic_libcontainers_unstable.gpg]\
    https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_$(lsb_release -rs)/ /" \
  | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:unstable.list > /dev/null
sudo apt-get update -qq
sudo apt-get -qq -y install podman
```

To aktualizovalo podmana a fungovalo to.
